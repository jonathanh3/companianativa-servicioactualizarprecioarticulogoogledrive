﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos
{
    public class DatosTango
    {
		public List<PrecioArticuloTango> TraerPreciosModificados(int numeroListaPrecio)
		{
			SqlDataAdapter da = null;
			DataTable dt = new DataTable();
			string consulta = string.Empty;
			List<PrecioArticuloTango> listPrecioArticuloTango = new List<PrecioArticuloTango>();
			try
			{
				// PRIMERA PARTE DEL UNION TRAIGO INSERT LUEGO UPDATE
				consulta =
					@"
						SELECT 
							DatosTango.CodigoArticulo, 
							DatosTango.Descripcion,
							DatosTango.DescripcionAdicional,
							DatosTango.Precio,
							DatosTango.FechaModificacion,
							HASHBYTES
							(
								'MD5',
								CONCAT
								(
									DatosTango.CodigoArticulo, 
									DatosTango.Descripcion,
									DatosTango.DescripcionAdicional,
									DatosTango.Precio,
									DatosTango.FechaModificacion
								)
							) AS HashTable
						FROM
						(
							SELECT 
								STA11.COD_ARTICU AS CodigoArticulo, 
								STA11.DESCRIPCIO AS Descripcion,
								STA11.DESC_ADIC AS DescripcionAdicional,
								GVA17.PRECIO AS Precio,
								GVA17.FECHA_MODI AS FechaModificacion
							FROM GVA17
							INNER JOIN STA11 ON STA11.COD_ARTICU = GVA17.COD_ARTICU
							WHERE
								GVA17.NRO_DE_LIS = @NumeroListaPrecio AND 
								STA11.COD_ARTICU NOT LIKE '%*%'
						) AS DatosTango
						LEFT JOIN SEIN_SYNC_PRECIO_ARTICULO ON DatosTango.CodigoArticulo = SEIN_SYNC_PRECIO_ARTICULO.CodigoArticulo
						WHERE
							HASHBYTES
							(
								'MD5',
								CONCAT
								(
									DatosTango.CodigoArticulo, 
									DatosTango.Descripcion,
									DatosTango.DescripcionAdicional,
									DatosTango.Precio,
									DatosTango.FechaModificacion
								)
							) <> SEIN_SYNC_PRECIO_ARTICULO.HashTable OR
							SEIN_SYNC_PRECIO_ARTICULO.CodigoArticulo IS NULL
				";
				da = new SqlDataAdapter(consulta, ConfigurationManager.AppSettings.Get("CadenaConexion"));
				da.SelectCommand.Parameters.Add("@NumeroListaPrecio", SqlDbType.Int).Value = numeroListaPrecio;
				da.Fill(dt);

				foreach (DataRow row in dt.Rows)
				{
					var precioArticuloTango = new PrecioArticuloTango();

					precioArticuloTango.CodigoArticulo = Convert.ToString(row["CodigoArticulo"]);
					precioArticuloTango.Descripcion = Convert.ToString(row["Descripcion"]);
					precioArticuloTango.DescripcionAdicional = Convert.ToString(row["DescripcionAdicional"]);
					precioArticuloTango.FechaModificacion = Convert.ToDateTime(row["FechaModificacion"]);
					precioArticuloTango.Precio = Convert.ToDouble(row["Precio"]);

					listPrecioArticuloTango.Add(precioArticuloTango);
				}
			}
			catch
			{
				throw;
			}
			return listPrecioArticuloTango;
		}

		public void BorrarNoExistentesTablaSync(int numeroListaPrecio)
		{
			SqlCommand cm = new SqlCommand();
			try
			{
				using (var cn = new SqlConnection(ConfigurationManager.AppSettings.Get("CadenaConexion")))
				{
					cn.Open();
					using (var tran = cn.BeginTransaction())
					{

						cm = new SqlCommand();
						cm.Connection = cn;
						cm.Transaction = tran;

						cm.CommandText =
							@"
								DELETE FROM SEIN_SYNC_PRECIO_ARTICULO
								WHERE
									NOT EXISTS
									(
										SELECT
											1
										FROM 
										(
											SELECT 
												STA11.COD_ARTICU AS CodigoArticulo, 
												STA11.DESCRIPCIO AS Descripcion,
												STA11.DESC_ADIC AS DescripcionAdicional,
												GVA17.PRECIO AS Precio,
												GVA17.FECHA_MODI AS FechaModificacion
											FROM GVA17
											INNER JOIN STA11 ON STA11.COD_ARTICU = GVA17.COD_ARTICU
											WHERE
												GVA17.NRO_DE_LIS = @NumeroListaPrecio AND 
												STA11.COD_ARTICU NOT LIKE '%*%'
										) AS DatosTango
										WHERE
											DatosTango.CodigoArticulo = SEIN_SYNC_PRECIO_ARTICULO.CodigoArticulo
									)
							";
						cm.Parameters.Add("@NumeroListaPrecio", SqlDbType.Int).Value = numeroListaPrecio;
						cm.CommandTimeout = 0;
						cm.ExecuteNonQuery();

						tran.Commit();
						cn.Close();
					}
				}
			}
			catch { throw; }
		}

		public bool InsertarModificarHashTableArticulo(string codigoArticulo, int numeroListaPrecio)
		{
			bool guardo = false;
			SqlCommand cm = new SqlCommand();
			try
			{
				using (var cn = new SqlConnection(ConfigurationManager.AppSettings.Get("CadenaConexion")))
				{
					cn.Open();
					using (var tran = cn.BeginTransaction())
					{

						cm = new SqlCommand();
						cm.Connection = cn;
						cm.Transaction = tran;

						cm.CommandText =
							@"
								IF EXISTS(SELECT 1 FROM SEIN_SYNC_PRECIO_ARTICULO WHERE SEIN_SYNC_PRECIO_ARTICULO.CodigoArticulo = @CodigoArticulo)
									BEGIN
										UPDATE SEIN_SYNC_PRECIO_ARTICULO 
										SET
											HashTable = HASHBYTES
													(
														'MD5',
														CONCAT
														(
															DatosTango.CodigoArticulo, 
															DatosTango.Descripcion,
															DatosTango.DescripcionAdicional,
															DatosTango.Precio,
															DatosTango.FechaModificacion
														)
													)
										FROM SEIN_SYNC_PRECIO_ARTICULO 
										INNER JOIN 
										(
											SELECT 
												STA11.COD_ARTICU AS CodigoArticulo, 
												STA11.DESCRIPCIO AS Descripcion,
												STA11.DESC_ADIC AS DescripcionAdicional,
												GVA17.PRECIO AS Precio,
												GVA17.FECHA_MODI AS FechaModificacion
											FROM GVA17
											INNER JOIN STA11 ON STA11.COD_ARTICU = GVA17.COD_ARTICU
											WHERE
												GVA17.NRO_DE_LIS = @NumeroListaPrecio AND 
												STA11.COD_ARTICU NOT LIKE '%*%'
										) AS DatosTango ON
											DatosTango.CodigoArticulo = SEIN_SYNC_PRECIO_ARTICULO.CodigoArticulo
										WHERE 
											SEIN_SYNC_PRECIO_ARTICULO.CodigoArticulo = @CodigoArticulo
									END
								ELSE
									BEGIN
										INSERT INTO SEIN_SYNC_PRECIO_ARTICULO
										(
											CodigoArticulo, 
											HashTable
										)
										SELECT
											@CodigoArticulo,
											HASHBYTES
												(
													'MD5',
													CONCAT
													(
														DatosTango.CodigoArticulo, 
														DatosTango.Descripcion,
														DatosTango.DescripcionAdicional,
														DatosTango.Precio,
														DatosTango.FechaModificacion
													)
												) AS HashTable
										FROM
										(
											SELECT 
												STA11.COD_ARTICU AS CodigoArticulo, 
												STA11.DESCRIPCIO AS Descripcion,
												STA11.DESC_ADIC AS DescripcionAdicional,
												GVA17.PRECIO AS Precio,
												GVA17.FECHA_MODI AS FechaModificacion
											FROM GVA17
											INNER JOIN STA11 ON STA11.COD_ARTICU = GVA17.COD_ARTICU
											WHERE
												GVA17.NRO_DE_LIS = @NumeroListaPrecio AND 
												STA11.COD_ARTICU NOT LIKE '%*%' AND 
												STA11.COD_ARTICU = @CodigoArticulo
										) AS DatosTango
									END
							";
						cm.Parameters.Clear();
						cm.Parameters.Add("@CodigoArticulo", SqlDbType.VarChar).Value = codigoArticulo;
						cm.Parameters.Add("@NumeroListaPrecio", SqlDbType.Int).Value = numeroListaPrecio;
						cm.ExecuteNonQuery();

						tran.Commit();
						cn.Close();
					}
				}
			}
			catch { throw; }
			return guardo;
		}
    }
}

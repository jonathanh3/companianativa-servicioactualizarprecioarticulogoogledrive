﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Auth.OAuth2.Responses;
using Google.Apis.Drive.v3;
using Google.Apis.Drive.v3.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Google.Apis.Drive.v3.DriveService;

namespace Datos
{
    public class GoogleDriveService
    {
        public static DriveService GetService()

        {
            var tokenResponse = new TokenResponse
            {
                AccessToken = "ya29.a0ARrdaM-pXC_5TNkSAw0KOtHiRt8WcbWIMZWsXvoVXanxk1eeLzJi8kwo1xgbi1jvRmwOcpNyAbITbEMhsqzL-WD8axCbdd3X0rIKtUhNtoXARVV-aeUr7_I-L_CH-xAS52X3yyb5VUUWUwbq2AMOi0_MxqaT",
                RefreshToken = "1//04NRButcwj1T6CgYIARAAGAQSNgF-L9Irdwp6u21J1u2nRRhosumAHKIVRKztDptFOyo8VGTrmgCb5hHyIrPryObuEz_DBydrEQ",
            };


            var applicationName = "DriveTangoProject";
            var username = "artsent.base.de.datos@gmail.com";


            var apiCodeFlow = new GoogleAuthorizationCodeFlow(new GoogleAuthorizationCodeFlow.Initializer
            {
                ClientSecrets = new ClientSecrets
                {
                    ClientId = "558932886852-jv90tta2nivp77q5ibq12545s9vfjuqb.apps.googleusercontent.com",
                    ClientSecret = "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"
                },
                Scopes = new[] { Scope.Drive },
                DataStore = new FileDataStore(applicationName)
            });


            var credential = new UserCredential(apiCodeFlow, username, tokenResponse);


            var service = new DriveService(new BaseClientService.Initializer
            {
                HttpClientInitializer = credential,
                ApplicationName = applicationName
            });
            return service;
        }
        public void UploadFile(Stream file, string fileName, string fileMime, string fileDescription,string folder)
        {
            DriveService service = GetService();


            var driveFile = new Google.Apis.Drive.v3.Data.File();
            driveFile.Name = fileName;
            driveFile.Description = fileDescription;
            driveFile.MimeType = fileMime;
            driveFile.Parents = new string[] { folder };
            
            
            var request = service.Files.Create(driveFile, file, fileMime);
            request.Fields = "id";
            

            var response = request.Upload();
            if (response.Status != Google.Apis.Upload.UploadStatus.Completed)
                throw response.Exception;
        }
        public void UpdateFile(string id,Stream stream,string contentType)
        {
            DriveService service = GetService();
            Google.Apis.Drive.v3.Data.File updatedFileMetadata = new Google.Apis.Drive.v3.Data.File();
                
                FilesResource.UpdateMediaUpload updateRequest;
                string fileId = id;
                
                
                    updateRequest = service.Files.Update(updatedFileMetadata, fileId, stream, contentType);
                   
                    var response=updateRequest.Upload();
                    if (response.Status != Google.Apis.Upload.UploadStatus.Completed)
                    throw response.Exception;

        }
        public IEnumerable<Google.Apis.Drive.v3.Data.File> GetFiles(string folder)
        {
            var service = GetService();

            var fileList = service.Files.List();
            fileList.Q = $"mimeType!='application/vnd.google-apps.folder' and '{folder}' in parents";
            fileList.Fields = "nextPageToken, files(id, name, size, mimeType)";

            var result = new List<Google.Apis.Drive.v3.Data.File>();
            string pageToken = null;
            do
            {
                fileList.PageToken = pageToken;
                var filesResult = fileList.Execute();
                var files = filesResult.Files;
                pageToken = filesResult.NextPageToken;
                result.AddRange(files);
            } while (pageToken != null);
   

    return result;
        }
       

    }
}

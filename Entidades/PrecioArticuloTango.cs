﻿using System;

namespace Entidades
{
    public class PrecioArticuloTango
    {
        public string CodigoArticulo { get; set; }
        public string Descripcion { get; set; }
        public string DescripcionAdicional { get; set; }
        public DateTime FechaModificacion { get; set; }
        public double Precio { get; set; }
        
    }
}
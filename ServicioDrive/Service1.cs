﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Timers;
using System.Windows.Forms;

namespace ServicioDrive
{
    public partial class Service1 : ServiceBase
    {
        public const int minuto_en_miliseg = 60000;
        System.Timers.Timer timer;

        public Service1()
        {
            try
            {
                InitializeComponent();
                //System.Diagnostics.Debugger.Launch();
                timer = new System.Timers.Timer();
                timer.Interval = Convert.ToDouble(ConfigurationManager.AppSettings.Get("MinutosEjecucion")) * minuto_en_miliseg;
                timer.Elapsed += new System.Timers.ElapsedEventHandler(Ejecutar);
            }
            catch (Exception ex)
            {
                LogErrores(ex);
            }
        }
        
        private void LogErrores(Exception ex, string codigoArticulo = "")
        {
            using (StreamWriter oLog = new StreamWriter(Application.StartupPath + "\\Errores.log", true))
            {
                if (codigoArticulo != "")
                    oLog.WriteLine(DateTime.Now.ToString("dd-MM-yyy HH:mm") + "Cód artículo: " + codigoArticulo);

                oLog.WriteLine(DateTime.Now.ToString("dd-MM-yyy HH:mm") + "Message: " + ex.Message);
                oLog.WriteLine(DateTime.Now.ToString("dd-MM-yyy HH:mm") + "InnerException: " + ex.InnerException);
                oLog.WriteLine(DateTime.Now.ToString("dd-MM-yyy HH:mm") + "StackTrace: " + ex.StackTrace);
            }
        }

        private void Ejecutar(object sender, ElapsedEventArgs e)
        {
            timer.Stop();
            try
            {
                int numeroListaPrecio = Convert.ToInt32(ConfigurationManager.AppSettings.Get("NumeroListaPrecio"));
                string idFolder = Convert.ToString(ConfigurationManager.AppSettings.Get("idFolder"));

                Datos.DatosTango datos = new Datos.DatosTango();
                var gds = new Datos.GoogleDriveService();

                datos.BorrarNoExistentesTablaSync(numeroListaPrecio);
                
                List<Entidades.PrecioArticuloTango> listaPrecioArticuloTango = datos.TraerPreciosModificados(numeroListaPrecio);

                List<Google.Apis.Drive.v3.Data.File> listaFiles = (List<Google.Apis.Drive.v3.Data.File>)gds.GetFiles(idFolder);

                foreach (var precioArticuloTango in listaPrecioArticuloTango.ToList())
                {
                    ReportDocument rpt = new ReportDocument();

                    try
                    {                   
                        rpt.Load(Application.StartupPath + @"\Reportes\rptPrecioArticulo.rpt");
                        rpt.SetParameterValue("CodigoArticulo", precioArticuloTango.CodigoArticulo);
                        rpt.SetParameterValue("Descripcion", precioArticuloTango.Descripcion);
                        rpt.SetParameterValue("DescripcionAdicional", precioArticuloTango.DescripcionAdicional);
                        rpt.SetParameterValue("FechaModi", precioArticuloTango.FechaModificacion);
                        rpt.SetParameterValue("Precio", precioArticuloTango.Precio);
                        rpt.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Application.StartupPath + @"\PDF\" + precioArticuloTango.CodigoArticulo + ".pdf");

                        var stream = new System.IO.FileStream(Application.StartupPath + @"\PDF\" + precioArticuloTango.CodigoArticulo + ".pdf", System.IO.FileMode.Open);
                        
                        var archivosExistentes = listaFiles.Where(f => f.Name.Equals(precioArticuloTango.CodigoArticulo) || f.Name.Equals(precioArticuloTango.CodigoArticulo + ".pdf")).ToList();
                        if (archivosExistentes.Count == 0)
                            gds.UploadFile(stream, precioArticuloTango.CodigoArticulo, "application/pdf", "", idFolder);
                        else
                        {
                            foreach(var file in archivosExistentes)
                                gds.UpdateFile(file.Id, stream, "application/pdf");
                        }

                        datos.InsertarModificarHashTableArticulo(precioArticuloTango.CodigoArticulo, numeroListaPrecio);
                    }
                    catch (Exception ex)
                    {
                        LogErrores(ex, precioArticuloTango.CodigoArticulo);
                    }
                    finally
                    {
                        rpt.Dispose();
                    }
                }
                               
                using (TextWriter tw = new StreamWriter(Application.StartupPath + @"\UltimoProceso.txt", false, Encoding.ASCII))
                {
                    tw.WriteLine(DateTime.Now);
                }
            }
            catch (Exception ex)
            {
                LogErrores(ex);
            }
            timer.Start();
        }
        protected override void OnStart(string[] args)
        {
            try
            {
                //Debugger.Launch();
                timer.Start();
            }
            catch (Exception ex)
            {
                LogErrores(ex);
            }
        }

        protected override void OnStop()
        {
            try
            {
                timer.Stop();
            }
            catch (Exception ex)
            {
                LogErrores(ex);
            }
        }
    }
}
